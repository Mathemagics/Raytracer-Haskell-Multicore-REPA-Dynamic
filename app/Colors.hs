module Colors where
import Graphics.Gloss.Raster.Array

data Material = Material | Glass | Water | Vacuum | Copper | Green | Purple deriving Show
refractiveIndex Glass = 1.470
refractiveIndex Water = 1.330
refractiveIndex Vacuum = 1.0


color Glass =  rgbI' 0  0  0
color Water =  rgbI' 0 175 175 
color Copper = rgbI'  255 0 0
color Vacuum = rgbI'  255  255 255
color Green  = rgbI'  0  255 0
color Purple = rgbI' 102 0  204
