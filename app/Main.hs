module Main where

--import Lib
import Display
import Graphics.Gloss.Raster.Array

main :: IO ()
main = animateArrayIO window (4,4) (generateFrame)
  where
    window = (InWindow "Testing" (800,800) (0,0))
    backgroundColor = white

--main = generateFrame 0
