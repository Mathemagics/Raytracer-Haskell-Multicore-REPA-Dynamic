{-# LANGUAGE Strict #-}
module Geometry where
import Data.List
import Colors as Col

data Sphere = Sphere {center::Vertex,
                      radius::Float,
                      sMat::Material}
            deriving Show
             
data Ray = Ray {origin::Vertex,
                direction::Vertex}
         deriving Show

data Pyramid = Pyramid {apex::Vertex,                     
                        base::Quadrilateral}
             deriving Show

data Quadrilateral = Quadrilateral {p1::Vertex,
                                    p2::Vertex,
                                    p3::Vertex,
                                    p4::Vertex}
                   deriving Show

data Fustrum = Fustrum {pyramid::Pyramid, apexPlane::Plane}

data Plane = Plane {displacement::Float,
                    normal::Vector}
           deriving Show

--A vector in R^3 (Position), has a defined cross product.
data Vertex = Vertex {x::Float, y::Float, z::Float} deriving Show    
instance Num (Vertex) where
  (Vertex x y z) + (Vertex u v w) = Vertex (x+u) (y+v) (z+w)
  (Vertex x y z) - (Vertex u v w) = Vertex (x-u) (y-v) (z-w)
  (Vertex x y z) * (Vertex u v w) = Vertex (x*u) (y*v) (z*w)
  abs (Vertex x y z) = Vertex (abs x) (abs y) (abs z)
  negate (Vertex x y z) = Vertex (-x) (-y) (-z)
  fromInteger x = Vertex (fromIntegral x) (fromIntegral x) (fromIntegral x)
  
  
--Eucledian Vector in R^3 (Magnitude)
data Vector = Vector {vTail::Vertex, vHead::Vertex} deriving Show
instance Num (Vector) where
  (Vector vTail vHead) + (Vector wTail wHead) = Vector {vTail=vTail, vHead = (wHead - (wTail-vHead))}
  v@(Vector vTail vHead) - w@(Vector wTail wHead) = v+(negate w)
  negate (Vector vTail vHead) = Vector vTail (negate vHead)
  abs (Vector vTail vHead) = Vector (abs vTail) (abs vHead)
  
dotProduct :: Vertex -> Vertex -> Float
dotProduct (Vertex x y z) (Vertex u v w) = x*u + y*v + z*w
crossProduct :: Vertex -> Vertex -> Vertex
crossProduct (Vertex x y z) (Vertex u v w) = Vertex (y*w-z*v) (x*w-u*z) (x*v-u*y)
scalarProduct :: Float -> Vertex -> Vertex
scalarProduct a (Vertex x y z) = Vertex (x*a) (y*a) (z*a)
distance :: Vertex -> Vertex -> Float
distance a@(Vertex _ _ _) b@(Vertex _ _ _) = sqrt (dotProduct (a-b) (a-b))


---------------------------------------------------------------------------------------------------------------

evalRay  _ (Nothing, object) = (Nothing, object)
evalRay (Ray origin direction) ( (Just time), object ) =  (Just $ (timeDisplacement time), object)
  where 
  timeDisplacement = (\t-> origin +  scalarProduct t direction)

quadFormula :: (Floating p, Eq p) => p -> p -> p -> [Maybe p]
quadFormula a b c
  | signum discriminant == (-1)  = [Nothing]  --Miss
  | signum discriminant == 0 =  [root1]    --Tangent
  | signum discriminant == 1 =  [root1,root2] --Penetration
  where
    discriminant = (b**2)-(4*a*c)
    root1 = Just $  ((-b)+(sqrt discriminant)) / (2*a)
    root2 = Just $  ((-b)-(sqrt discriminant)) / (2*a)

rayIntersect :: Ray -> Sphere -> [(Maybe Float, Sphere)]
rayIntersect (Ray origin direction)  object@(Sphere center radius _) = [(t,object) | t <- times]
  where
    a = dotProduct direction direction
    b = 2*dotProduct (origin-center) (direction)
    c = dotProduct (center) (center) + dotProduct (origin) (origin) -
        2 * dotProduct (origin) (center) - (radius**2)
    times = quadFormula a b c

getClosest :: Vertex -> [(Maybe Vertex, b)] -> (Maybe Vertex, b)
getClosest referenceVertex@(Vertex _ _ _ ) vertices = head $ sortBy (compareClosest referenceVertex) vertices
  
compareClosest ::
  Vertex -> (Maybe Vertex, b1) -> (Maybe Vertex, b2) -> Ordering
compareClosest referenceVertex@(Vertex _ _ _) ((Just v), object1) ((Just u), object2)
    | (distance v referenceVertex) > (distance u referenceVertex) = GT
    | (distance v referenceVertex) < (distance u referenceVertex) = LT

--This should push Nothing to the tail, be careful though. A filter would make more sense?
compareClosest referenceVertex@(Vertex _ _ _) ((Nothing), object1) ((Just u), object2) = GT
compareClosest referenceVertex@(Vertex _ _ _) ((Just v), object1) ((Nothing), object2) = LT
compareClosest referenceVertex@(Vertex _ _ _) ((Nothing), object1) ((Nothing), object2) = EQ
compareClosest (Vertex _ _ _) (Just _, _) (Just _, _) = EQ

    
