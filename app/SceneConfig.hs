module SceneConfig where
import Geometry as Geo
import Colors as Col
import Control.Applicative

camera :: Vertex
camera = Vertex 0 0 200

s1 :: Float -> Sphere
s1  time  = (Sphere {center = (Vertex 10 40 (-40) ), radius = (100), sMat=Copper}) --intersect
s2 time = (Sphere {center = (Vertex 70 40 (90*(sin time))), radius = 50, sMat=Water}) --closer intersect
s3 time = (Sphere {center = (Vertex ((sin time) *120) 64 ((cos time) * 152)), radius = 100, sMat=Green}) --no intersect
s4 time = (Sphere {center = (Vertex 10 20 30), radius = 60, sMat=Copper}) --closest
s5 time = (Sphere {center = (Vertex ((sin time)* 200) ((cos time) * 200) ((tan time) * 90)), radius = 50, sMat=Purple})

--objects :: [Float -> Sphere]
objects = [s1,s2,s3,s4,s5]


