{-# LANGUAGE Strict #-}
module Display (generateFrame) where

import Data.Array.Repa as Repa 
import Data.Array.Repa.Repr.Vector
import Graphics.Gloss
import Graphics.Gloss.Raster.Array
import Geometry as Geo
import qualified SceneConfig as Scene
import qualified Colors as Col

generateFrame :: Monad m => Float -> m (Array D DIM2 Color)
generateFrame time = do
   return deployedRays
   where
     changedObjects :: [Sphere]
     changedObjects = Prelude.map ($time) (Scene.objects) --This is some neat haskell right here.

     -- Place the ray's origin at the pixel center to puncture it.
     -- It then points the ray to the camera, and negates it, so it points into the scene at the appropriate angle.
     -- Offset the x,y coordinates so that the zero is in the center 
     rayArray :: Array V DIM2 Ray
     rayArray = fromListVector (Z :.  (displayWidth+1) :. (displayHeight+1)) frameWindow :: Array V DIM2 (Ray)
       where
         displayWidth :: Int
         displayWidth  = 200 
         displayHeight :: Int
         displayHeight = 200 
         
         maxDomain :: Float
         maxDomain = 300 ::Float
         maxRange :: Float
         maxRange =  300 ::Float
         minRange :: Float
         minRange = (-maxRange)
         minDomain :: Float
         minDomain = (-maxDomain)
         
         stepX :: Float
         stepX = ((maxRange-minRange) / (fromIntegral displayWidth))
         stepY :: Float
         stepY =  ((maxDomain-minDomain) / (fromIntegral displayHeight)) 
         
         frameWindow = [Ray {origin = Vertex ((-x) - 0.5)  ((-y) - (0.5))  0, direction = negate Scene.camera}|  x <- [minDomain, (minDomain + stepX)..maxDomain], y <- [minRange, (minRange+stepY)..maxRange]]

     frameRenderMonad :: Vertex -> [Sphere] -> Ray -> Color
     frameRenderMonad camera changedObjects ray 
       | isNothing closestIntersectVertices == True  = Col.color Col.Vacuum
       | isNothing closestIntersectVertices == False = returnColor
       where
         timeIntersects :: [[(Maybe Float, Sphere)]]
         timeIntersects = Prelude.map (\obj->rayIntersect ray obj) changedObjects

         vertices :: [(Maybe Vertex, Sphere)]
         vertices = Prelude.map (evalRay ray) (concat timeIntersects)

         closestIntersectVertices :: (Maybe Vertex, Sphere)
         closestIntersectVertices = getClosest camera vertices
         
         returnColor :: Color
         returnColor = Col.color (sMat $ snd closestIntersectVertices)

         isNothing :: (Maybe a, b) -> Bool
         isNothing (Nothing, _) = True
         isNothing (Just _ , _) = False

     --map
     deployedRays :: Array D DIM2 (Color)
     deployedRays =  Repa.map (frameRenderMonad Scene.camera changedObjects) rayArray 

     -- --compute rays
     -- computerayarray :: IO (Array U DIM2 (Color))
     -- computeRayArray = computeP deployRays :: IO(Array U DIM2 (Color))




