# Purpose

This is a raytracer written in Haskell using the REPA library to provide parallel processing. It utilizes Gloss for OpenGL animation, and is an improvement over my previous iteration, which only produced a static image. 

# Build and Execute
I do not suggest you do this. Install stack-> stack build -> stack execute raytracer-exe

# Contents
Please see the "app" directory for source code. All other files are used by stack.

# Mathematics 
This program takes a domain and range on the 3d cartersian plane, and projects it onto an array. This array is itself a projection of the monitor, with each element representing the center of a pixel on a computer monitor. 

A light particle is projected from each pixel, and travels into the 3d space. The collision, if any, is calculated using 3d parametric equations, returning a color based on the impacted object's properties. 

Some objects are now in periodic motion.

# To do:

1. Shading
2. Lighting
3. Additional object types
4. Other effects
5. Use of Accell library to move computation into the GPU.
6. Optimization